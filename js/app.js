$(document).ready(function(){
    initPlayer();
    getSongs();
});
var audio = document.getElementById('player'); // reproducir y pausar las canciones
var music; //contiene lo valores    
function initPlayer(){
    $('#shuffle').click(function(){
        $('#playlist').empty();
        console.log(shuffle(music.songs));
        playSong(0);
        genList(music);
    })
}
function getSongs(){
    $.getJSON("js/app.json",function(mjson){
            music = mjson;   //music esta guardando el objeto json
            console.log(music);
            genList(music); // se va generar una lista
    });
}
function playSong(id){
    console.log(id);
    var long = music.songs;
    if(id>= long.length){
        console.log('se termino');
        audio.pause();
    }else{
    $('#img-album').attr('src',music.songs[id].image);
    $('#player').attr('src',music.songs[id].song);
    audio.play(music.songs);
    console.log('hay mas canciones');
    scheduleSong(id);
    }
}
function genList(music){
    console.log(music.songs);
    $.each(music.songs,function(i,song){
        $('#playlist').append('<li class="list-group-item" id="'+i+'">'+song.name+'</i>');
    });
    $('#playlist li').click(function(){
        var selectedsong = $(this).attr('id');
        playSong(selectedsong);
    });
}
function scheduleSong(id){
    audio.onended = function(){
        console.log('Fin de la cancion');
        playSong(parseInt(id)+1);
    }
}
function shuffle (array){
    for(var random, temp, position = array.length; position; random = Math.floor(Math.random()*position), temp = array[--position], array[position]=array[random],array[random]= temp);
    return array;
}